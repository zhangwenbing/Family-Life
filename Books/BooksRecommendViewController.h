//
//  BooksRecommendViewController.h
//  Books
//
//  Created by apple on 12-5-14.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataService.h"
#import "PushService.h"
#import "AsyncDataLoadDowner.h"
@interface BooksRecommendViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tbApps;
@property (weak, nonatomic) IBOutlet UIButton *btnFree;
@property (weak, nonatomic) IBOutlet UIButton *btnHot;
@property (weak, nonatomic) IBOutlet UIButton *btnNecessary;
- (IBAction)OnFreeClick:(id)sender;
- (IBAction)OnHotClick:(id)sender;

- (IBAction)OnNecessaryClick:(id)sender;
-(void) reLoadTableView;
 @end
