//
//  BooksModelController.h
//  Books
//
//  Created by apple on 12-3-10.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BooksBaseViewController.h"
@class BooksDataViewController;

@interface BooksModelController : NSObject <UIPageViewControllerDataSource>
{
    NSString *bookKey;
}
- (id)viewControllerAtIndex:(NSUInteger)index storyboard:(UIStoryboard *)storyboard;
- (NSUInteger)indexOfViewController:(BooksBaseViewController *)viewController;
@property (strong, nonatomic) NSString *bookKey;
@end
