//
//  BooksDataViewController.h
//  Books
//
//  Created by apple on 12-3-10.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BooksBaseViewController.h"

@interface BooksDataViewController : BooksBaseViewController
@property (weak, nonatomic) IBOutlet UITextView *txtContents;
 
@property (weak, nonatomic) IBOutlet UILabel *lblPageIndex;
 
@property (strong, nonatomic) IBOutlet UILabel *dataLabel;
@property (weak, nonatomic) IBOutlet UIButton *btnSaveBookMark;
- (IBAction)BtnSaveBookMarkClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *savabook;

@end
