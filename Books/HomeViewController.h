//
//  HomeViewController.h
//  Books
//
//  Created by apple on 12-3-10.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "DMAdView.h"	
@interface HomeViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
{    NSDictionary *tableData;
//DMAdView *_dmAdView;
    float version;

}
@property (weak, nonatomic) IBOutlet UIImageView *backImage;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (retain) NSDictionary *tableData;
@end
