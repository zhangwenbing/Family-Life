//
//  BookMarkViewController.h
//  Books
//
//  Created by apple on 12-6-3.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataService.h"
@interface BookMarkViewController : UITableViewController
{

    float version;
}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *mianBtn;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *deletebtn;
 
- (IBAction)ClearBookMarkClick:(id)sender;
 
@property (weak, nonatomic) IBOutlet UITableView *tbData;

@end
