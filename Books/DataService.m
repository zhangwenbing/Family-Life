//
//  DataService.m
//  Books
//
//  Created by apple on 12-3-10.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "DataService.h"

@implementation DataService

//static  NSString * CurrentKey;

static NSMutableDictionary *dicBooksList;
 static NSString * gBookKey;
static NSInteger spageIndex;

+(NSInteger)GetBookPageIndex
{
    return spageIndex;
}
+(void)SetBookPageIndex:(NSInteger)pageIndex
{
    spageIndex=pageIndex;
}
+(void)SetBookKey:(NSString *)bookKey
{
    gBookKey=bookKey;
}
 +(NSString *)GetBookKey
{
    return gBookKey;
}
static  NSMutableDictionary * appconfig;

+(void)SetAppsConfig:(NSMutableDictionary *)appConfigs
{
    appconfig=appConfigs;
}
+(NSMutableDictionary *)GetAppsConfigs
{
    if(appconfig ==nil)
    {
        //从本地文件缓存获得
        //resource path
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *IPDBfile = [documentsDirectory stringByAppendingPathComponent:@"config.plist"];
        //load data
        NSLog(@"%@",IPDBfile);
        
        appconfig = [NSMutableDictionary dictionaryWithContentsOfFile:IPDBfile];
        
    }
    
   return  appconfig;
}


+(NSMutableDictionary *)DicBooksList
{
 
if(!dicBooksList)
{
    
    //resource path
    NSBundle * mainBundle = [NSBundle mainBundle];
    NSString *IPDBfile=[mainBundle pathForResource:@"Books" ofType:@"plist"];
    //load data
    NSLog(@"%@",IPDBfile);
    
    dicBooksList = [NSMutableDictionary dictionaryWithContentsOfFile:IPDBfile];
    
    
}

return dicBooksList; 
  
    
}
static NSMutableDictionary *dicBook;
 static NSString * currentCacheBookKey;
+(NSMutableDictionary *) GetBooksbyId:(NSString *)bookKey
{
    //resource path
    if(dicBook==nil||currentCacheBookKey!=bookKey)
    {
    
    NSBundle * mainBundle = [NSBundle mainBundle];
    NSString *IPDBfile=[mainBundle pathForResource:bookKey ofType:@"plist"];
    dicBook = [NSMutableDictionary dictionaryWithContentsOfFile:IPDBfile];
    }
    
    return dicBook;
}
//获得总页数
+(NSUInteger ) GetPageCount:(NSString *)bid
{
   
    
    
     
    NSDictionary * book = [DataService GetBooksbyId: bid];
    NSUInteger  totalcount =0;
  
    if(book)
    {
        NSArray * chapters=[book objectForKey:Chapters]; 
      
    for (NSDictionary *chapter in chapters) {
        NSArray * pages=[chapter objectForKey:Pages];
        
        totalcount += pages.count;
    }
    }else {
        NSLog(@"没有数据");
    }
    
    return totalcount;
} 
+(void)InsertAdPage:(int)index book:(NSString *)bid data:(NSDictionary *)data
{
   
    
    NSMutableDictionary * book = [DataService GetBooksbyId: bid];
   // NSLog(@"%@",book);
    int count =0;
    NSArray * chapters=[book objectForKey:Chapters]; 
    for (NSDictionary *chapter in chapters) {
        
        NSMutableArray * pages=[chapter objectForKey:Pages];
        
        int  pagecount=pages.count;
        
        count +=  pagecount;
        
        if(count>=index+1)
        {
            int pageindex =count-index-1;
            pageindex=pagecount-pageindex-1;
     
            [pages insertObject:data atIndex:pageindex];
            //NSLog(@"%@",[pages objectAtIndex:pageindex]);
            break;
        } 
        
    }
}

//通过页码获得数据
+(NSDictionary *)GetPageData:(int) index :(NSString *)bid
{
    
   
    
    NSDictionary * book = [DataService GetBooksbyId: bid];
    NSMutableDictionary* result =[[NSMutableDictionary alloc] init];
    int count =0;
        NSArray * chapters=[book objectForKey:Chapters]; 
     for (NSDictionary *chapter in chapters) {

         NSArray * pages=[chapter objectForKey:Pages];
         
         int  pagecount=pages.count;
         
         count +=  pagecount;
       
         if(count>=index+1)
         {
             int pageindex =count-index-1;
             pageindex=pagecount-pageindex-1;
             [result setObject:[chapter objectForKey:Title] forKey:Title];
             NSNumber * nCType=[[pages objectAtIndex:pageindex] objectForKey:CType];
             //NSLog(@"%@-%@",[pages objectAtIndex:pageindex],bid);
             id strDetails =[[pages objectAtIndex:pageindex] objectForKey:Details];
             
             [result setObject:nCType forKey:CType];
             [result setObject:strDetails forKey:Details];
             break;
         } 
         
    }
return result;

}


+(NSString *)GetPageTitle:(int) index :(NSString *)bid
{
    
   
    
    NSDictionary * book =[DataService GetBooksbyId: bid];
    NSString* result;
    int count =0;
    NSArray * chapters=[book objectForKey:Chapters]; 
    for (NSDictionary *chapter in chapters) {
        
        NSArray * pages=[chapter objectForKey:Pages];
        
        int  pagecount=pages.count;
        
        count +=  pagecount;
        
        if(count>=index+1)
        {
            int pageindex =count-index-1;
            pageindex=pagecount-pageindex-1;
            NSLog(@"pageindex=%d,index=%d,count=%d",pageindex,index,count);
            result= [chapter objectForKey:Title];
  
            break;
        } 
        
    }
    return result;
    
}


+(void)SetBookMark:(int) index :(NSString *)bid
{
    NSMutableArray * bookmark=[DataService GetBookMark];
    
    [bookmark insertObject:[[NSArray alloc] initWithObjects:bid,[NSString stringWithFormat:@"%d" ,index], nil] atIndex:0];
    
    //save
    NSString * bookMarkFile= @"BookMarks";
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    bookMarkFile = [documentsDirectory stringByAppendingPathComponent:bookMarkFile];
      
    [bookmark writeToFile:bookMarkFile atomically:true];
}

+(NSMutableArray *)GetBookMark
{
 NSString * bookMarkFile= @"BookMarks";
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    bookMarkFile = [documentsDirectory stringByAppendingPathComponent:bookMarkFile];
    NSLog(@"%@",bookMarkFile);
    NSFileManager * fm =[NSFileManager defaultManager];
    
    if([fm fileExistsAtPath:bookMarkFile]==NO )
    {
        return [[NSMutableArray alloc] init];
    }else {
        return [[NSMutableArray alloc] initWithContentsOfFile:bookMarkFile];
    }
}

+(void)ClearBookMark
{
    NSMutableArray * bookmark=[[NSMutableArray alloc] init];
    NSString * bookMarkFile= @"BookMarks";
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    bookMarkFile = [documentsDirectory stringByAppendingPathComponent:bookMarkFile];
    
    [bookmark writeToFile:bookMarkFile atomically:true];
}
@end
