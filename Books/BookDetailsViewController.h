//
//  BookDetailsViewController.h
//  Books
//
//  Created by apple on 12-3-11.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncDataLoadDowner.h"
@interface BookDetailsViewController : UIViewController
{
    float version;

}
@property (weak, nonatomic) IBOutlet UIImageView *imgCover;
@property (weak, nonatomic) IBOutlet UILabel *lblAuthor;
@property (weak, nonatomic) IBOutlet UITextView *txtDetails;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnRead;
- (IBAction)OnBtnReadClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnPause;
- (IBAction)OnBtnPauseClick:(id)sender;

@end
