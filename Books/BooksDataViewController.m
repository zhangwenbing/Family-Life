//
//  BooksDataViewController.m
//  Books
//
//  Created by apple on 12-3-10.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "BooksDataViewController.h"
#import "DataService.h"
@interface BooksDataViewController ()

@end

@implementation BooksDataViewController

@synthesize txtContents = _txtContents;
@synthesize lblPageIndex = _lblPageIndex;

@synthesize dataLabel = _dataLabel;
@synthesize btnSaveBookMark = _btnSaveBookMark;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    if ([[[UIDevice currentDevice] systemVersion] intValue] >6 ) {
        
      
        [self.btnSaveBookMark setFrame:CGRectMake(124, 437, 72, 34)];
        [self.lblPageIndex setFrame:CGRectMake(206, 437, 106, 21)];
        if (self.view.frame.size.height == 568) {
            [self.dataLabel setFrame:CGRectMake(0, 74, 320, 24)];
            
             [self.txtContents setFrame:CGRectMake(0, 108, 320, 359)];
            [self.lblPageIndex setFrame:CGRectMake(206, 457, 106, 21)];
            self.savabook.frame = CGRectMake(124, 500, 72, 34);
            
        }else if (self.view.frame.size.height == 480){
            [self.dataLabel setFrame:CGRectMake(0, 74, 320, 24)];
            
             [self.txtContents setFrame:CGRectMake(0, 98, 320, 359)];
            [self.lblPageIndex setFrame:CGRectMake(206, 457, 106, 21)];
        
        }
       
    }else{
   
    [self.txtContents setFrame:CGRectMake(0, 24, 320, 359)];
    }
    
    
    
}

- (void)viewDidUnload
{
    //NSLog(@"BooksDataViewControllerviewDidUnload");
    [self setTxtContents:nil];
    self.dataLabel = nil;
    [self setLblPageIndex:nil];
    self.dataContents=nil;
    self.dataIndex=nil;
    [self setBtnSaveBookMark:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    int pageindex=self.dataIndex.integerValue+1;
    UIColor * colordata ;
    /*
    float fcolormax=255.0;
    float red=arc4random() % 45 + 201;
    float green=arc4random() % 45 + 201;
    float blue=arc4random() % 45 + 201;
     */
    
    colordata=[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:243.0/255.0 alpha:1];
    
    UIView * backView=  self.view;
    NSLog(@"%@_%d",colordata,pageindex%5);
    backView.backgroundColor=colordata;
    
    self.dataLabel.text=[self.dataContents objectForKey:Title];
    self.txtContents.text=[self.dataContents objectForKey:Details];
    self.lblPageIndex.text=[NSString stringWithFormat:@"<%d/%d>",pageindex,[DataService GetPageCount:[DataService GetBookKey]]];
}
-(void) viewDidDisappear:(BOOL)animated
{
    
    [self viewDidUnload];
    
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (IBAction)BtnSaveBookMarkClick:(id)sender {
    
    [DataService SetBookMark:[self.dataIndex  intValue] :[DataService GetBookKey]];
}
@end
