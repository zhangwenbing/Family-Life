//
//  BooksBaseViewController.h
//  Books
//
//  Created by apple on 12-3-23.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BooksBaseViewController : UIViewController<UIGestureRecognizerDelegate>
@property (strong, nonatomic) NSNumber * dataIndex;
@property (strong, nonatomic) NSDictionary * dataContents;
@end
