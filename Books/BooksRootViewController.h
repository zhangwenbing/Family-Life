//
//  BooksRootViewController.h
//  Books
//
//  Created by apple on 12-3-10.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BooksRootViewController : UIViewController <UIPageViewControllerDelegate>
 
@property (strong, nonatomic) UIPageViewController *pageViewController;

@end
