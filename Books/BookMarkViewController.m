//
//  BookMarkViewController.m
//  Books
//
//  Created by apple on 12-6-3.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "BookMarkViewController.h"

@interface BookMarkViewController ()

@end

@implementation BookMarkViewController
@synthesize tbData;
 
 

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    version = [[[UIDevice currentDevice] systemVersion] floatValue];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"shangmian.png"] forBarMetrics:UIBarMetricsDefault];
    
    
    if ((int)version >6) {
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        
    }else{
    self.mianBtn.tintColor = [UIColor colorWithRed:149.0/255.0 green:98.0/255.0 blue:50.0/255.0 alpha:1];
        self.deletebtn.tintColor =[UIColor colorWithRed:149.0/255.0 green:98.0/255.0 blue:50.0/255.0 alpha:1];
        UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
        backItem.title =@"书签";
        [backItem setTintColor:[UIColor colorWithRed:149.0/255.0 green:98.0/255.0 blue:50.0/255.0 alpha:1]];
        
        self.navigationItem.backBarButtonItem = backItem;
    
    }
    


    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;

}

- (void)viewDidUnload
{
 
 
    [self setTbData:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
-(void) viewWillAppear:(BOOL)animated
{   
    [self.tbData reloadData];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
 
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
 
    NSArray * bookMark=[DataService GetBookMark];
    
    int count =0;
    if(bookMark!=nil)
    {
        count=bookMark.count;
    }
    NSLog(@"count%d",count);
    // Return the number of rows in the section.
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
      UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] ;
  cell.selectionStyle=UITableViewCellSelectionStyleNone;
    // Configure the cell...
    NSMutableArray * bookmarks=[DataService GetBookMark];
 
    NSArray * bookArray=[ bookmarks objectAtIndex:indexPath.row];
    
    NSString * bookKey =[bookArray objectAtIndex:0];
    NSDictionary * dicData =[[DataService DicBooksList] objectForKey:bookKey];
  
    cell.textLabel.text=[NSString stringWithFormat:@"第%d页 %@", [[bookArray objectAtIndex:1] intValue]+1,[dicData objectForKey:Title]  ];
    return cell;
}

 
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
 

 
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
   
        NSMutableArray * bookmarks=[DataService GetBookMark];
        
        [bookmarks removeObjectAtIndex:indexPath.row];
        NSString * bookMarkFile= @"BookMarks";
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        bookMarkFile = [documentsDirectory stringByAppendingPathComponent:bookMarkFile];
        
        [bookmarks writeToFile:bookMarkFile atomically:true];
        [self.tbData reloadData];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
 

 
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
 

 
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
 

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray * bookmarks=[DataService GetBookMark];
    
    NSArray * bookArray=[ bookmarks objectAtIndex:indexPath.row];
    [DataService SetBookKey:[bookArray objectAtIndex:0]];
    NSInteger count =[[bookArray objectAtIndex:1] intValue];
 
    [DataService SetBookPageIndex:count];
    
    [self performSegueWithIdentifier:@"sgDetails" sender:self];
}

- (IBAction)ClearBookMarkClick:(id)sender {
    
    [DataService ClearBookMark];
    [self.tbData reloadData];
}
@end
