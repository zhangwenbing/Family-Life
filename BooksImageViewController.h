//
//  BooksImageViewController.h
//  Books
//
//  Created by apple on 12-3-23.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BooksBaseViewController.h"

@interface BooksImageViewController : BooksBaseViewController{
 CGFloat lastScale;
   //UIImageView *imageView;
    //UIView *holderView;
}
@property (weak, nonatomic) IBOutlet UIImageView *imgContents;
 
-(void)scale:(id)sender;
 
 
@end
