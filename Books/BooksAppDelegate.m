//
//  BooksAppDelegate.m
//  Books
//
//  Created by apple on 12-3-10.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "BooksAppDelegate.h"
#import "PushService.h"
#import "AsyncDataLoadDowner.h"
#import <AVOSCloud/AVOSCloud.h>
#import "ShowAdvertisement.h"

@implementation BooksAppDelegate

@synthesize window = _window;

- (NSString *)appKey{
    return UMENG_KEY; //吸血鬼
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    

    [MobClick startWithAppkey:@"5417e620fd98c565aa096c2b"];
    ShowAdvertisement *test = [[ShowAdvertisement alloc] init];
    test.target = test;
    UIView *adView = [test showAdvertisement:@"5417e207e4b05e378ee5852e"];

    [AVOSCloud setApplicationId:@"b0qe00mcn6q49cba699sxs0kxigw288j184r2tl55hwc7qk2"
                      clientKey:@"34q04tdgiokz0aj0us6oy8snbt2uob15ryaojq8pm8o3j9wb"];
    [AVAnalytics trackAppOpenedWithLaunchOptions:launchOptions];

    AsyncDataLoadDowner *configInfo =[[AsyncDataLoadDowner alloc] init];
    
    [configInfo loadConfigFromURL:[NSURL URLWithString:  SERVER_AD_URL]];

    //注册用户
    BOOL isFirst = [[NSUserDefaults standardUserDefaults] boolForKey:@"isfirstrun"];
    if(!isFirst)
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isfirstrun"];
        CFUUIDRef   uuidObj = CFUUIDCreate(nil);//create a new UUID
        //get the string representation of the UUID
        NSString  *uuidString = (__bridge_transfer NSString*)CFUUIDCreateString(nil, uuidObj);
        NSString *uuid;
        uuid = uuidString;
        
        
        NSUserDefaults *saveUUID = [NSUserDefaults standardUserDefaults];
        [saveUUID setValue:uuid forKey:@"UUID"];
        
       
    }
    /*
    //注册Push
    
    /*
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge|UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeSound)];
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
   
    // 处理收到的push信息
    NSDictionary* aps = [[launchOptions objectForKey:@"UIApplicationLaunchOptionsRemoteNotificationKey"] objectForKey:@"aps"];
    if(aps)
    {
        [PushService deal_push:[launchOptions objectForKey:@"UIApplicationLaunchOptionsRemoteNotificationKey"] 
			application:application appDelegate:self
         ];
    }
     */

       
    // Override point for customization after application launch.

    
//    [MobClick setDelegate:self reportPolicy:BATCH];

    /*
    [MobClick setDelegate:self reportPolicy:BATCH];
>>>>>>> 5c9824dfc82cbae9408684cd25078b1d3c1f87b2
    AsyncDataLoadDowner*   configLoadDowner = [[AsyncDataLoadDowner alloc] init];
    NSDate * now=[NSDate date];
    
   
    NSDateFormatter * dateFormater=[[NSDateFormatter alloc] init];
    [dateFormater setDateStyle:NSDateFormatterShortStyle];
    [dateFormater setDateFormat:@"YYYYMMdd"];
     NSLog(@"current%@",[dateFormater stringFromDate:now]);
    
    
    NSURL * urlconfigLoadDowner =[[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@?t=%@", SERVER_APP_URL,[dateFormater stringFromDate:now]]];
    
     [configLoadDowner loadOtherConfigFromURL:urlconfigLoadDowner];
     
     */
    [application registerForRemoteNotificationTypes:
     UIRemoteNotificationTypeBadge |
     UIRemoteNotificationTypeAlert |
     UIRemoteNotificationTypeSound];
    [self.window.rootViewController.view addSubview:adView];
    
    return YES;
}
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    AVInstallation *currentInstallation = [AVInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    [currentInstallation saveInBackground];
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

// 处理对话框点击
- (void) alertView:(UIAlertView *)alert clickedButtonAtIndex:(NSInteger)buttonIndex
{ 
    
    //push about
    // 推送
    if ( buttonIndex == 1) {
        [PushService processNotification:[PushService GetUserinfo]];
    }
    
}
/*
//注册Push
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken 
{
    NSLog(@"%@",@"token success");
    NSString *sdeviceToken = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    sdeviceToken = [sdeviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"%@",sdeviceToken);
    [PushService registerPush:sdeviceToken];
}
- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    NSLog(@"注册Push失败%@",err);

    
}
//收到Push
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSLog(@"userInfo:%@",userInfo);
    
    [PushService deal_push:userInfo
        application:application appDelegate:self];
}
//end push about
 */

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
