//
//  BooksAdViewController.m
//  Books
//
//  Created by apple on 12-3-29.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "BooksAdViewController.h"
#import "DataService.h"
@interface BooksAdViewController ()

@end

@implementation BooksAdViewController
@synthesize uiImage;
@synthesize btnAdImage;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}
- (void)loadView
{
    [super loadView];
    // If you create your views manually, you MUST override this method and use it to create your views.
    // If you use Interface Builder to create your views, then you must NOT override this method.
}
- (void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];

    NSDictionary * dic= [self.dataContents objectForKey:Details];
    
    NSString * imageurl =[dic objectForKey:AdImage];
        
    
    AsyncDataLoadDowner * imageDown = [[AsyncDataLoadDowner alloc] init];
    [imageDown  loadImageFromURL:[NSURL URLWithString:imageurl] image:self.uiImage];
    
    //初始化 图片 
    
    /*
    NSURL * url =[NSURL URLWithString:imageurl];
    
    UIImage *image =[UIImage  imageWithData:[NSData dataWithContentsOfURL:url]];
 
    [self.btnAdImage setBackgroundImage:image forState:UIControlStateNormal];
    */
   
    
}

- (void)viewDidUnload
{
    [self setBtnAdImage:nil];
    [self setUiImage:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)OnAdImageClick:(id)sender {
    NSDictionary * dic= [self.dataContents objectForKey:Details];
    
    NSString * url =[dic objectForKey:AdUrl];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}
@end
