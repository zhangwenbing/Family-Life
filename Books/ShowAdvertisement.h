//
//  ShowAdvertisement.h
//  AdvertisementSDK
//
//  Created by APPYING on 14-7-21.
//  Copyright (c) 2014年 APPYING. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVOSCloud/AVOSCloud.h>

//设备屏幕尺寸
#define kScreen_Height   ([UIScreen mainScreen].bounds.size.height)
#define kScreen_Width    ([UIScreen mainScreen].bounds.size.width)
#define kScreen_Frame    (CGRectMake(0, 0 ,kScreen_Width,kScreen_Height))
#define kScreen_CenterX  kScreen_Width/2
#define kScreen_CenterY  kScreen_Height/2

@interface ShowAdvertisement : UIViewController {
    UIView *returnView;//广告背景层
    UIImageView *advertiseImgView;//广告图片
    NSString *urlAdvertisement;//广告的url地址
}

@property (strong, nonatomic) id target;

- (UIView *)showAdvertisement:(NSString *)appId;
@end
