//
//  BookDetailsViewController.m
//  Books
//
//  Created by apple on 12-3-11.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "BookDetailsViewController.h"
#import "DataService.h"
@interface BookDetailsViewController()
@property (weak, nonatomic) IBOutlet UIButton *readBtn;
@property (weak, nonatomic) IBOutlet UIButton *lookBtn;

@end

@implementation BookDetailsViewController
@synthesize btnPause;
@synthesize imgCover;
@synthesize lblAuthor;
@synthesize txtDetails;
@synthesize lblTitle;
@synthesize btnRead;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)loadView
{
    [super loadView];
    // If you create your views manually, you MUST override this method and use it to create your views.
    // If you use Interface Builder to create your views, then you must NOT override this method.
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     version = [[[UIDevice currentDevice] systemVersion] floatValue];
     
    if ((int)version >6 ) {
        
        [self.lblTitle setFrame:CGRectMake(10, 74, 320, 20)];
        [self.imgCover setFrame:CGRectMake(120, 114, 100, 140)];
        [self.lblAuthor setFrame:CGRectMake(70, 274, 190, 20)];
        [self.txtDetails setFrame:CGRectMake(20, 304, 280, 120)];
        if (self.view.frame.size.height == 568) {
            
            [self.readBtn setFrame:CGRectMake(33, 458, 113, 38)];
            [self.lookBtn setFrame:CGRectMake(174, 458, 113, 38)];
        }else if(self.view.frame.size.height == 480){
    
            [self.readBtn setFrame:CGRectMake(33, 428, 113, 38)];
            [self.lookBtn setFrame:CGRectMake(174, 428, 113, 38)];
           
        }
        
    }else{
        UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
        backItem.title =@"返回";
        [backItem setTintColor:[UIColor colorWithRed:149.0/255.0 green:98.0/255.0 blue:50.0/255.0 alpha:1]];
        
        self.navigationItem.backBarButtonItem = backItem;
    
    }
	// Do any additional setup after loading the view, typically from a nib.
    
}

- (void)viewDidUnload
{
    [self setImgCover:nil];
    [self setLblAuthor:nil];
    [self setTxtDetails:nil];
    [self setLblTitle:nil];
    
    [self setBtnRead:nil];
    [self setBtnPause:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}
-(void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
}
//填充控件
-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UIColor * colordata ;
    /*
    float fcolormax=255.0;
    float red=arc4random() % 45 + 201;
    float green=arc4random() % 45 + 201;
    float blue=arc4random() % 45 + 201;
     */
    
    colordata=[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:243.0/255.0 alpha:1];
    self.view.backgroundColor=colordata;
    
    NSString *bookKey =[DataService GetBookKey];
    // NSLog(@"gBookKey%@",bookKey);
    NSDictionary * dicData = [[DataService DicBooksList] objectForKey:bookKey];
    //封面
    NSBundle * mainBundle = [NSBundle mainBundle];
    NSString *coverPath=[mainBundle  pathForAuxiliaryExecutable:[dicData objectForKey:Cover]];
    UIImage *image =[UIImage imageWithContentsOfFile:coverPath];
    self.imgCover.image=image;
    //作者
    self.lblAuthor.text=[NSString stringWithFormat:@"作者：%@",[dicData objectForKey:Author]];
    //简介
    self.txtDetails.text=[dicData objectForKey:Details];
    self.lblAuthor.backgroundColor=[UIColor clearColor];
    self.txtDetails.backgroundColor=[UIColor clearColor];
    //导航标题
    
    self.lblTitle.text =[dicData objectForKey:Title];
    
    self.title=[NSString stringWithFormat:@"%@",BookTitle];
    UIViewController * leftBar=  [self.navigationController.viewControllers objectAtIndex:0];
    
    leftBar.title=@"返回";
    [DataService SetBookPageIndex:0];
    
    AsyncDataLoadDowner * audioDowner = [AsyncDataLoadDowner GetAudioPlayer];
    //初始化播放按钮
    if(audioDowner.isPlayStat)
    {
        
        [self.btnPause  setTitle:@"暂停" forState:UIControlStateNormal];
        
    }else {
        
        
        [self.btnPause  setTitle:@"播放" forState:UIControlStateNormal];
        
    }
    NSLog(@"btnPause%@",self.btnPause.titleLabel.text);
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (IBAction)OnBtnReadClick:(id)sender {
    AsyncDataLoadDowner * audioDowner = [AsyncDataLoadDowner GetAudioPlayer];
    [audioDowner AudioInit];
    [audioDowner PlayAudioText];
    
    audioDowner.isPlayStat=true;
    [self.btnPause  setTitle:@"暂停" forState:UIControlStateNormal];
}

- (IBAction)OnBtnPauseClick:(id)sender {
    AsyncDataLoadDowner * audioDowner = [AsyncDataLoadDowner GetAudioPlayer];
    if(audioDowner.isInitPlayer)
    {
        if(audioDowner.isPlayStat)
        {
            [audioDowner AudioPause];
            
            
            [btnPause  setTitle:@"播放" forState:UIControlStateNormal];
            NSLog(@"%@",btnPause.titleLabel.text);
            audioDowner.isPlayStat=false;
        }else {
            [audioDowner AudioPlay];
            
            [btnPause  setTitle:@"暂停" forState:UIControlStateNormal];
            NSLog(@"%@",btnPause.titleLabel.text);
            audioDowner.isPlayStat=true;
        }
    }else {
        
        [audioDowner AudioInit];
        [audioDowner PlayAudioText];
        
        audioDowner.isPlayStat=true;
        [self.btnPause  setTitle:@"暂停" forState:UIControlStateNormal];
    }
}
@end
