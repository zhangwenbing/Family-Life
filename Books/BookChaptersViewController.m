//
//  BookChaptersViewController.m
//  Books
//
//  Created by apple on 12-3-11.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "BookChaptersViewController.h"
#import "DataService.h"
@interface BookChaptersViewController ()

@end

@implementation BookChaptersViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    version = [[[UIDevice currentDevice] systemVersion] floatValue];
    if ((int)version <= 6) {
        UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
        backItem.title =@"目录";
        [backItem setTintColor:[UIColor colorWithRed:149.0/255.0 green:98.0/255.0 blue:50.0/255.0 alpha:1]];
        
        self.navigationItem.backBarButtonItem = backItem;
    }

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
 
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString * bookKey =[DataService GetBookKey];
    NSDictionary * dicData =[DataService GetBooksbyId: bookKey];
    NSInteger count =[[dicData objectForKey:Chapters] count];
    //NSLog(@"%d",count);
    return  count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    NSString * bookKey =[DataService GetBookKey];
    NSDictionary * dicData =[DataService GetBooksbyId: bookKey];
    NSArray * chapters = [dicData objectForKey:Chapters];
 
    
    cell.textLabel.text=[NSString stringWithFormat:@"%@",[[chapters objectAtIndex:indexPath.row] objectForKey:Title]];
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/
//填充控件
-(void) viewWillAppear:(BOOL)animated
{   
self.title=@"目录";
    UIViewController * leftBar=  [self.navigationController.viewControllers objectAtIndex:1];
   
    leftBar.title=@"返回";
}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    NSString * bookKey =[DataService GetBookKey];
    NSDictionary * dicData =[DataService GetBooksbyId: bookKey];
    NSArray * chapters = [dicData objectForKey:Chapters];
    NSInteger count =0;
    for (int i=0; i<indexPath.row; i++) {
        count+=[[[chapters objectAtIndex:i] objectForKey:Pages] count];
    }
    [DataService SetBookPageIndex:count];
    
    [self performSegueWithIdentifier:@"sgDetails" sender:self];
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}
-(void) viewDidDisappear:(BOOL)animated
{
    [self viewDidUnload];
    
}
@end
