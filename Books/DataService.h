﻿//
//  DataService.h
//  Books
//
//  Created by apple on 12-3-10.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
 
static const NSString* Chapters=@"chapters";
static const NSString* Chapter=@"chapter";
static const NSString* Pages=@"pages";
static const NSString* Title=@"title";
static const NSString* Details=@"details";
static const NSString* CType=@"ctype";
static const NSString* Index=@"index";
static const NSString* Cover=@"cover";
static const NSString* Author=@"author";
static const NSString* BookTitle=@"家庭生活必备图书合集";
static const NSString* AdUrl=@"adurl";
static const NSString* AdImage=@"adimage";

@interface DataService : NSObject
+(NSInteger)GetBookPageIndex;
+(void)SetBookPageIndex:(NSInteger)pageIndex;
 +(NSString *)GetBookKey;

+(void)SetBookKey:(NSString *)bookKey;
+(NSMutableDictionary *)DicBooksList;
+(NSMutableDictionary *) GetBooksbyId:(NSString *)bookKey;
+(NSUInteger ) GetPageCount:(NSString *)bid;
+(NSDictionary *)GetPageData:(int) index :(NSString *)bid;
+(NSString *)GetPageTitle:(int) index :(NSString *)bid;
+(void)InsertAdPage:(int)index book:(NSString *)bid data:(NSDictionary *)data;

 +(void)SetAppsConfig:(NSMutableDictionary *)appConfigs;
+(NSMutableDictionary *)GetAppsConfigs;

//bookmark
+(void)SetBookMark:(int) index :(NSString *)bid;
+(NSMutableArray *)GetBookMark;

+(void)ClearBookMark;
@end
