//
//  BookCityViewController.m
//  ShelfTxtReaderDaoMuBiJimogo
//
//  Created by iOS One on 14-8-6.
//  Copyright (c) 2014年 Huo Shuqiang. All rights reserved.
//

#import "BookCityViewController.h"


@interface BookCityViewController ()

@end

@implementation BookCityViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     
    version = [[[UIDevice currentDevice] systemVersion] floatValue];
    [[UIApplication sharedApplication]setStatusBarHidden:YES];
    
    self.view.backgroundColor = [UIColor whiteColor];
    //百度小说
    NSString* string = @"http://m.baidu.com/book?tj=ios_baidu_app&ref=ios_baidu_app";
    NSURL* url = [NSURL URLWithString:string];
    NSURLRequest* request = [NSURLRequest requestWithURL:url];
    
    if ((int)version <= 6) {
        _webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, 320, 435)];
    }else{
        if (self.view.frame.size.height == 568) {
            _webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, 320, 523)];
        } else {
            _webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, 320, 435)];
        }
    }
    
    
    UIButton* btn = [[UIButton alloc]initWithFrame:CGRectMake(0, _webView.frame.size.height, 320, 45)];
    [btn setTitle:@"书架" forState:UIControlStateNormal];
    btn.backgroundColor = [UIColor colorWithRed:235.0/255 green:107.0/255 blue:51.0/255 alpha:1];
    [btn addTarget:self action:@selector(tap) forControlEvents:UIControlEventTouchUpInside];
    
    
    [_webView loadRequest:request];
    [self.view addSubview:_webView];
    [self.view addSubview:btn];
    
}
- (void)tap
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}@end
