//
//  BooksRecommendViewController.m
//  Books
//
//  Created by apple on 12-5-14.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "BooksRecommendViewController.h"

@interface BooksRecommendViewController ()

@end

@implementation BooksRecommendViewController
@synthesize tbApps;
@synthesize btnFree;
@synthesize btnHot;
@synthesize btnNecessary;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [self setTbApps:nil];
    [self setBtnFree:nil];
    [self setBtnHot:nil];
    [self setBtnNecessary:nil];
   
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}
AsyncDataLoadDowner * configLoadDowner ;
-(void) viewDidAppear:(BOOL)animated
{  
    NSMutableDictionary * AppsConfigs =[DataService GetAppsConfigs];
    tbApps.delegate=self;
  
    if(AppsConfigs==nil)
    {
      configLoadDowner = [[AsyncDataLoadDowner alloc] init];
        NSURL * url =[[NSURL alloc] initWithString:SERVER_APP_URL];
        NSData * data =[NSData dataWithContentsOfURL:url];
        NSDictionary *myDict = [NSPropertyListSerialization propertyListFromData:data mutabilityOption:NSPropertyListImmutable format:nil errorDescription:nil];
        NSLog(@"datanone");
        [DataService SetAppsConfig:(NSMutableDictionary *)myDict];
              [self reLoadTableView];
        
    }else{
        
        [self reLoadTableView];
    }
    
}
NSInteger rowNumber =0;
NSString * appKey=@"1001";
NSArray * appDatas;

-(void) reLoadTableView
{
   
    NSMutableDictionary * AppsConfigs =[DataService GetAppsConfigs];
    if(AppsConfigs!=nil)
    {   NSLog(@"tuijian2");
        NSArray * listApps = [AppsConfigs objectForKey:appKey]; 
        rowNumber =[listApps count];
        appDatas=listApps;
        tbApps.dataSource=self;
        [tbApps reloadData];
    }
} 


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   
    return  rowNumber;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] ;
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    // Configure the cell...
    NSArray * appRow=[appDatas objectAtIndex:indexPath.row] ;
   //icon
     CGRect  rectImage =  CGRectMake(12, 12, 57, 57);
    UIImageView * icon = [[UIImageView alloc] initWithFrame:rectImage];
     AsyncDataLoadDowner * configLoadDowner = [[AsyncDataLoadDowner alloc] init];
    NSURL * iconUrl = [NSURL URLWithString:[appRow objectAtIndex:0]];
    [configLoadDowner loadImageFromURL:iconUrl image:icon];
    [cell addSubview:icon];
//title
    CGRect  rectTitle =  CGRectMake(78, 12, 170, 24);
    UILabel * title = [[UILabel alloc] initWithFrame:rectTitle];
    title.text=[appRow objectAtIndex:1];
     title.font =[UIFont  fontWithName:@"LiSu" size:23];
     [cell addSubview:title];
    
    //stars
    NSBundle * mainBundle = [NSBundle mainBundle];
    NSString *starPath=[mainBundle  pathForAuxiliaryExecutable:@"star.png"];
    UIImage * imgStar =[UIImage imageWithContentsOfFile:starPath];

    NSString * strcount =[appRow objectAtIndex:2];
    NSInteger starCount =  [strcount integerValue];

    for (NSInteger i =0; i<starCount; i++) {
          CGRect  rectstrImage =  CGRectMake(77+i*14+2, 39, 14, 14);
        UIImageView * strView =  [[UIImageView alloc] initWithFrame:rectstrImage];
        strView.image=imgStar;
          [cell addSubview:strView];
    }
    //details
    CGRect  rectDetails =  CGRectMake(78, 57, 170, 23);
    UILabel * details = [[UILabel alloc] initWithFrame:rectDetails];
    details.text=[appRow objectAtIndex:3];
        details.font =[UIFont  fontWithName:@"LiSu" size:15];
     [cell addSubview:details];
    //price
    CGRect  rectprice =  CGRectMake(257, 15, 53, 17);
    UILabel * price = [[UILabel alloc] initWithFrame:rectprice];
    price.text=[appRow objectAtIndex:4];
    [cell addSubview:price];
    //link
    CGRect  rectlink =  CGRectMake(255, 45, 58, 29);
    
    UIButton * link =[[UIButton alloc] initWithFrame:rectlink];
    [link setTitle:[appRow objectAtIndex:5] forState:UIControlStateNormal];
    
        NSString *linkPath=[mainBundle  pathForAuxiliaryExecutable:@"bj.jpg"];
    UIImage * imglink =[UIImage imageWithContentsOfFile:linkPath];

            [link setBackgroundImage:imglink forState:UIControlStateNormal];
    [link setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    [link addTarget:self action:@selector(onAppClick:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [link setTag: indexPath.row];
      [cell addSubview:link];
    
    
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray * appRow=[appDatas objectAtIndex:indexPath.row] ;
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[appRow objectAtIndex:6]]];
}
-(void) onAppClick:(id)sender
{
    
       NSArray * appRow=[appDatas objectAtIndex:[sender tag]] ;
  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[appRow objectAtIndex:6]]];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)OnFreeClick:(id)sender {
    appKey=@"1001";
    [self reLoadTableView];
}

- (IBAction)OnHotClick:(id)sender {
    appKey=@"1002";
    [self reLoadTableView];
}
- (IBAction)OnNecessaryClick:(id)sender {
    appKey=@"1003";
    [self reLoadTableView];
}
@end
