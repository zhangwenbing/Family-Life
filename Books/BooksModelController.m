//
//  BooksModelController.m
//  Books
//
//  Created by apple on 12-3-10.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "BooksModelController.h"

#import "BooksDataViewController.h"
#import "DataService.h"
/*
 A controller object that manages a simple model -- a collection of month names.
 
 The controller serves as the data source for the page view controller; it therefore implements pageViewController:viewControllerBeforeViewController: and pageViewController:viewControllerAfterViewController:.
 It also implements a custom method, viewControllerAtIndex: which is useful in the implementation of the data source methods, and in the initial configuration of the application.
 
 There is no need to actually create view controllers for each page in advance -- indeed doing so incurs unnecessary overhead. Given the data model, these methods create, configure, and return a new view controller on demand.
 */

@interface BooksModelController()

@end

@implementation BooksModelController

@synthesize bookKey ;

- (id)init
{
    self = [super init];
    if (self) {
           }
    return self;
}

- (id)viewControllerAtIndex:(NSUInteger)index storyboard:(UIStoryboard *)storyboard
{   
    // Return the data view controller for the given index.
     NSUInteger pagescount =[DataService GetPageCount:self.bookKey ];
    
    if ((pagescount == 0) || (index >= pagescount)) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    NSDictionary * dataContents= [DataService  GetPageData:index   :self.bookKey ];
    
    NSNumber * nCType= [dataContents objectForKey:CType];
    NSString * contollerName ;
    NSInteger type=[nCType unsignedIntegerValue];
    if( type==1)
    {
        contollerName=@"BooksDataViewController";
    }else  if(type ==2) 
    {
        contollerName=@"BooksImageDataViewController";
    } else if(type==3)
    {
        contollerName=@"BooksAdViewController";
    }else
    {
        contollerName=@"BooksDataViewController";
    }
    
    NSLog(@"%@",contollerName);
    BooksBaseViewController *dataViewController = [storyboard instantiateViewControllerWithIdentifier:contollerName];
   // NSLog(@"initIndex=%i",index);
    dataViewController.dataContents =dataContents;
    
    dataViewController.dataIndex=[NSNumber numberWithInt:index];
   
    return dataViewController;
}

- (NSUInteger)indexOfViewController:(BooksBaseViewController *)viewController
{   
     // Return the index of the given data view controller.
     // For simplicity, this implementation uses a static array of model objects and the view controller stores the model object; you can therefore use the model object to identify the index.
    NSNumber * index=viewController.dataIndex;
    //NSLog(@"current=%d",[index unsignedIntegerValue]);
    return [index unsignedIntegerValue];
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
     
    NSUInteger index = [self indexOfViewController:(BooksBaseViewController *)viewController];
  
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index storyboard:viewController.storyboard];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = [self indexOfViewController:(BooksBaseViewController *)viewController];
    
    if (index == NSNotFound) {
        NSLog(@"NSNotFound");
        return nil;
    }
    
    index++;
    NSUInteger  pagescount =[DataService GetPageCount:self.bookKey ];
  //  NSLog(@"index=%d=pagescount=%d", index,pagescount);
   // NSLog(@"index=%i,pagescount=%i",index,(NSInteger)pagescount);
    if (index ==  pagescount) {
         
        return nil;
    }
    return [self viewControllerAtIndex:index storyboard:viewController.storyboard];
}


@end
