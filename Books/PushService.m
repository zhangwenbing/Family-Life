//
//  PushService.m
//  Books
//
//  Created by apple on 12-3-28.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "PushService.h"
 


@implementation PushService
static NSDictionary * Userinfo;
static NSURLConnection* connection; 
+(NSDictionary *)GetUserinfo{
    return Userinfo;
}

+(void)SetUserinfo:(NSDictionary *) userinfo{
      Userinfo=userinfo;
}
//push about
+ (void) registerPush:(NSString *) adeviceID
{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init]; 
  

    
    NSUserDefaults *UUID = [NSUserDefaults standardUserDefaults];
    NSLog(@"%@",UUID);
    NSString *uuid = [UUID valueForKey:@"UUID"];
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
    [dict setObject:HEADER_CUSTOMER_ID forKey:@"hc"];
    [dict setObject:@"hmd" forKey:@"hmd"];
    [dict setObject:@"hsz" forKey:@"hsz"];
    [dict setObject:HEADER_VERSION forKey:@"hv"];
    [dict setObject:@"hsv" forKey:@"hsv"];
    [dict setObject:uuid forKey:@"hu"];
    [dict setObject:HEADER_USER_AGENCY forKey:@"ha"];
    [dict setObject:@"ham" forKey:@"ham"];
    [dict setObject:@"19" forKey:@"hi"];
    [dict setObject:adeviceID forKey:@"adeviceId"];
                         
 
    //header
    for (NSString *key in [dict allKeys]) {
        // NSLog(@"%@",key);
          [request setValue:[dict objectForKey:key] forHTTPHeaderField:key];  
    }
    //url
    NSURL * url =[NSURL URLWithString:SERVER_URL];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    
  
    if(connection ==nil)
    {
        NSLog(@"%@",request);
        connection = [[NSURLConnection alloc] initWithRequest:request delegate:nil]; 
    }
   
 
}
//end push about

//push about
// 处理push, 无论是程序启动或未启动，收到的push信息都会由这里处理
+ (BOOL) deal_push:(NSDictionary *)userInfo application:(UIApplication *)application appDelegate:(BooksAppDelegate *) appDelegate
{
    /*
     实例
     1.转到频道
     {"aps":{"alert":"章子怡常说，觉得自己就是一个意外。在《时尚COSMO》3月号，她展示了这个“意外”的前前后后。点击免费下载《时尚COSMO3》月号。","badge":1},"a":"2||1","t":"男人装"}
     2.转到URL
     {"aps":{"alert":"章子怡常说，觉得自己就是一个意外。在《时尚COSMO》3月号，她展示了这个","badge":1},"a":"1||1","t":"男人装","u":"430755988"}
     
    self.NotificationUserInfo = userInfo;
    
    NSArray* params = [self getCombinArrayFromNotification];
    if(params)
    {
        // 记录push
        [self logPush:[params objectAtIndex:1] opType:@"1"]; //opType 1 means choose yes by user
    }
    */
    // 程序活动，显示提示
    if(application.applicationState == UIApplicationStateActive) 
    {
        NSLog(@"recive RemoteNotification when application active %@",[userInfo description]);
        NSDictionary *aps = [userInfo objectForKey:@"aps"];
        NSString *message = [aps objectForKey:@"alert"];
        NSString *title = [userInfo objectForKey:@"t"];
        if (message) 
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                                message:message  delegate:appDelegate
                                                      cancelButtonTitle:@"取消"
                                                      otherButtonTitles:@"显示", nil];
            [PushService SetUserinfo:userInfo];
          
            [alertView show];
          
            
            [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
            
             
        }
    } 
    // 程序非活动，直接处理
    else 
    {
        NSLog(@"recive RemoteNotification when application inactive %@",[userInfo description]);
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
        [self processNotification:userInfo];
        //return [self processNotification];
    }
    
    return NO;
}

//push about
+ (BOOL) processNotification:(NSDictionary *)userInfo
{
	 
    NSString *combinArray = [userInfo objectForKey:@"a"];
    NSString *type = [combinArray substringToIndex:1];
	NSString *appid =[userInfo objectForKey:@"u"];
	NSString *urls;
    
    NSLog(@"type%@",type);
	if ([appid hasPrefix:@"http://"]) 
    {
		urls = appid;
	}
    else 
    {
		urls = [NSString stringWithFormat:@"http://itunes.apple.com/cn/app/id%@?l=en&amp;mt=8",appid];
	}
	
    if ([type intValue] == 1) 
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urls]];
    }
    else if ([type intValue] == 2) 
    {
        return NO;
    } 
    else 
    {
        
    }
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
   
    // 框架已处理此push，不需要项目处理
    return YES;
}
//end push about
@end
