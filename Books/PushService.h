//
//  PushService.h
//  Books
//
//  Created by apple on 12-3-28.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BooksAppDelegate.h"
//online server
#define SERVER_URL @"http://tvxq.ann9.com/service.ashx"
#define SERVER_AD_URL @"http://tvxq.ann9.com/ad/2002.xml"
#define SERVER_APP_URL @"http://tvxq.ann9.com/App/2002.xml"
//for http header
#define HEADER_CUSTOMER_ID     @"2002"
#define HEADER_VERSION         @"1.0.0"
#define HEADER_USER_AGENCY     @"lishujie"
#define DOMOB_AD_KEY         @"56OJz9tYuMtzDhtPyu"
#define UMENG_KEY    @"4ff3ffa65270154b3700001c"
@interface PushService : NSObject
 
+ (void) registerPush:(NSString *) adeviceID;
+ (BOOL) deal_push:(NSDictionary *)userInfo application:(UIApplication *)application appDelegate:(BooksAppDelegate *) appDelegate;
+(NSDictionary *)GetUserinfo;
+(void)SetUserinfo:(NSDictionary *) userinfo;
+ (BOOL) processNotification:(NSDictionary *)userInfo;
@end
