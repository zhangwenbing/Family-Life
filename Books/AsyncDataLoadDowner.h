//
//  AsyncImageLoadDowner.h
//  Books
//
//  Created by apple on 12-3-29.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CommonCrypto/CommonDigest.h>
#import "DataService.h"
#import <AVFoundation/AVFoundation.h>  
@interface AsyncDataLoadDowner : NSObject<AVAudioPlayerDelegate>
{
       NSURLConnection* connection; 
       NSMutableData* data;
    UIImageView * uiImage;
    NSString * urlfile;
    NSInteger dtype;
    bool isInitPlayer;
    bool isPlayStat;
    
}
@property  bool isInitPlayer;
@property  bool isPlayStat;
- (void)loadImageFromURL:(NSURL*)url image:(UIImageView *) button;
- (void)loadConfigFromURL:(NSURL*)url  ;
- (void)loadAudioFromURL:(NSURL*)url ;
-(NSString *) createdMD5:(NSString *) signString;
-(void)ProcessAdData;
-(void)ProcessImage;
-(void)ProcessAudioData;
-(NSString *)GetAudioPageText;
-(void) AudioInit;
-(void) PlayAudioText;
-(void) AudioPause;
-(void) AudioPlay;
+(AsyncDataLoadDowner *)GetAudioPlayer;
- (void)loadOtherConfigFromURL:(NSURL*)url ;

-(void)ProcessConfigData;
-(void)ProcessAdData;
-(void)PlayAudioData;

@end
