//
//  BooksImageViewController.m
//  Books
//
//  Created by apple on 12-3-23.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "BooksImageViewController.h"
#import "DataService.h"
@interface BooksImageViewController ()

@end

@implementation BooksImageViewController
@synthesize imgContents;
 
 

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)loadView
{
     [super loadView];
    // If you create your views manually, you MUST override this method and use it to create your views.
    // If you use Interface Builder to create your views, then you must NOT override this method.
}
- (void)viewWillAppear:(BOOL)animated
{
        
    [super viewWillAppear:animated];
 
    
    //初始化 图片 
    NSBundle * mainBundle = [NSBundle mainBundle];
    NSString *coverPath=[mainBundle  pathForAuxiliaryExecutable:[self.dataContents objectForKey:Details]];
    UIImage *image =[UIImage imageWithContentsOfFile:coverPath];
    
    self.imgContents.image=image;
 /*
  UIView *rootView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 640, 640)];
  rootView.backgroundColor = [UIColor yellowColor];
  self.view = rootView;
    holderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 640, 960)];
    imageView = [[UIImageView alloc] initWithFrame:[holderView frame]];
    [imageView setImage:image];
    [holderView addSubview:imageView];
    //拧的手势
    UIPinchGestureRecognizer *pinchRecognizer = [[UIPinchGestureRecognizer alloc] 
                                                 initWithTarget:self action:@selector(scale:)];
    [pinchRecognizer setDelegate:self];
    [holderView addGestureRecognizer:pinchRecognizer];
    
    [rootView addSubview:holderView];
    */
    
    
     
    
    
  
}

- (void)scale:(id)sender {
 
 
    [self.view bringSubviewToFront:[(UIPinchGestureRecognizer*)sender view]];
 
    //当手指离开屏幕时,将lastscale设置为1.0
    if([(UIPinchGestureRecognizer*)sender state] == UIGestureRecognizerStateEnded) {
        lastScale = 1.0;
        return;
    }
    
    CGFloat scale = 1.0 - (lastScale - [(UIPinchGestureRecognizer*)sender scale]);
    CGAffineTransform currentTransform = [(UIPinchGestureRecognizer*)sender view].transform;
    CGAffineTransform newTransform = CGAffineTransformScale(currentTransform, scale, scale);
    
    [[(UIPinchGestureRecognizer*)sender view] setTransform:newTransform];
    
    lastScale = [(UIPinchGestureRecognizer*)sender scale];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer 
shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
 
    return ![gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]];
}

 

- (void)viewDidLoad
{
    [super viewDidLoad];

	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    
    [self setImgContents:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
      
    return YES;
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
