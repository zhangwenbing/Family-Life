//
//  BooksAdViewController.h
//  Books
//
//  Created by apple on 12-3-29.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BooksBaseViewController.h"
#import "AsyncDataLoadDowner.h"
@interface BooksAdViewController : BooksBaseViewController
@property (weak, nonatomic) IBOutlet UIButton *btnAdImage;
- (IBAction)OnAdImageClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *uiImage;

@end
